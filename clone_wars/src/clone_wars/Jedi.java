/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clone_wars;

/**
 *
 * @author YannAssah
 */
public class Jedi extends Player{
    
    String rank[] = {"Padawan", "Master", "Council Member"};

    public Jedi(){
        setHp(100);
        setRank(rank[0]);
        def = 200;
        power = 100;
        force = 200;
        intelligence = 200;
    }
    
    @Override
    public void armedAttack(ennemy foe){
        double hit = 35 + ((double)this.intelligence/ (double)foe.def)*10;
        int left = foe.getHp() - (int)hit;
        foe.setHp(left);
    }


    public void forceAttack(ennemy foe){
        int hit = (this.force/10)*2;
        int left = foe.getHp() - (int)hit;
        foe.setHp(left);
    }
}
