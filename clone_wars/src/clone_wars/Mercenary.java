/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clone_wars;

/**
 *
 * @author YannAssah
 */
public class Mercenary extends Player{
    
    String rank[] = {"Rookie", "Crusader", "Mandalore"};
    
    public Mercenary(){
        setHp(100);
        setRank(rank[0]);
        def = 350;
        power = 100;
        force = 0;
        intelligence = 100;
    }
    
    @Override
    public void armedAttack(ennemy foe){
        double hit = 38 + ((double)this.intelligence/ (double)foe.def)*10;
        int left = foe.getHp() - (int)hit;
        foe.setHp(left);
    }
    
    public void assassination(ennemy foe){
        double hit = 25 + (((double)this.intelligence + (double)this.power)/ (double)foe.def)*10;
        if(foe.getHp() > hit){
            int left = foe.getHp() - (int)hit;
            foe.setHp(left);
        }else
            foe.setHp(0);
    }
    
}