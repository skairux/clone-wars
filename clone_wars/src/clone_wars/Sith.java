/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clone_wars;

/**
 *
 * @author YannAssah
 */
public class Sith extends Player{
    
    String rank[] = {"Saber", "Master", "Lord"};
    
    public Sith(){
        setHp(100);
        setRank(rank[0]);
        def = 100;
        power = 200;
        force = 200;
        intelligence = 200;
    }
    
    @Override
    public void armedAttack(ennemy foe){
        double hit = 35 + ((double)this.intelligence/ (double)foe.def)*10;
        int left = foe.getHp() - (int)hit;
        foe.setHp(left);
    }


    public void forceAttack(Player foe){
        int hit = (this.force/10)*2;
        int left = foe.getHp() - (int)hit;
        foe.setHp(left);
    }
    
}