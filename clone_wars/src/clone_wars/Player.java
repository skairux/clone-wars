/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clone_wars;

/**
 *
 * @author YannAssah
 */
public abstract class Player {
    private     int           hp;
    private   String        rank;
    protected   int          def;
    protected   int        power;
    protected   int        force;
    protected   int intelligence;
    protected   int      lvl = 1;
    protected   int       xp = 0;
    
    //Basic constructor
    public Player(){
        this.lvl = 1;
        this.xp = 0;
    }
    
    //Setter methods
    public void setHp(int hp){
        if(hp >= 0)
            this.hp = hp;
        else
            this.hp = 0;
    }
    
    protected void setHp(Player player, int hp){
        if(hp >= 0)
            player.hp = hp;
        else
            player.hp = 0;
    }
    
    protected void upExp(int Exp){
        this.xp += Exp;
    }
    
    protected void setRank(String rank){
        this.rank = rank;
    }
    
    protected void upLvl(){
        this.lvl += 1;
        this.hp *= 1.5;
        this.power *= 1.5;
        this.force *= 1.5;
        this.intelligence *= 1.5;
    }
    
    protected void upLvl(Player player){
        player.lvl += 1;
        player.hp *= 1.5;
        player.power *= 1.5;
        player.force *= 1.5;
        player.intelligence *= 1.5;
    }
    
    //Getter methods
    public void getStats(){
        System.out.println("HP : "           + this.hp);
        System.out.println("Def : "          + this.def);
        System.out.println("Power : "        + this.power);
        System.out.println("Force : "        + this.force);
        System.out.println("Intelligence : " + this.intelligence);
        System.out.println("Lvl : "          + this.lvl);
        System.out.println("Current xp : "   + this.xp);
    }
    
    public void getStats(Player player){
        System.out.println("HP : "           + player.hp);
        System.out.println("Def : "          + player.def);
        System.out.println("Power : "        + player.power);
        System.out.println("Force : "        + player.force);
        System.out.println("Intelligence : " + player.intelligence);
        System.out.println("Lvl : "          + player.lvl);
        System.out.println("Current xp : "   + player.xp);
    }
    
    public int getHp(Player player){
        return player.hp;
    }
    
    public int getHp(){
        return this.hp;
    }
    
    public void showHp(Player player){
        System.out.println(player.hp);
    }
    
    public void showHp(){
        System.out.println(this.hp);
    }
   
    //Basic attacks methods
    public void physicalAttack(ennemy foe){
        double hit = 20 + ((double)this.force/ (double)foe.def)*10;
        if(foe.hp >= hit){
            System.out.println("Vous utiliser une attaque qui inflige " + hit + " de degats à " + foe);
            int left = foe.getHp() - (int)hit;
            foe.setHp(foe, left);
        }else
            foe.setHp(foe, 0);
    }
    
    public void armedAttack(ennemy foe){
        double hit = 30 + ((double)this.intelligence/ (double)foe.def)*10;
        if(foe.hp >= hit){
            System.out.println("une attaque avec votre sabre inflige " + hit + " de degats à " + foe);
            int left = foe.getHp() - (int)hit;
            foe.setHp(left);
        }else
            foe.setHp(0);
    }
}
