/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clone_wars;

/**
 *
 * @author YannAssah
 */
public class Jawa extends Player{
    
    String rank[] = {"Scavenger", "Warrior", "Chief"};
    
    public Jawa(){
        setHp(300);
        setRank(rank[0]);
        def = 400;
        power = 200;
        force = 0;
        intelligence = 500;
    }
    
}
