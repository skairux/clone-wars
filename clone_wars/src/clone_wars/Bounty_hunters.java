/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clone_wars;

/**
 *
 * @author YannAssah
 */
public class Bounty_hunters extends Player{
    
    String rank[] = {"Tracker", "Elite", "Nova"};
    
    public Bounty_hunters(){
        setHp(100);
        def = 100;
        power = 100;
        force = 0;
        intelligence = 350;
    }
    
    public void assassination(ennemy foe){
        double hit = 25 + (((double)this.intelligence + (double)this.power)/ (double)foe.def)*10;
        if(foe.getHp() > hit){
            System.out.println("Vous lancer une attaque furtive et invliger" + hit + " points de degats à " + foe.name);
            int left = foe.getHp() - (int)hit;
            foe.setHp(left);
        }else
            foe.setHp(0);
    }
    
}