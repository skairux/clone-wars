/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clone_wars;
import java.io.*;
import javax.sound.sampled.*;

/**
 *
 * @author andria_m
 */
public class SongGame {
    
    public static synchronized void chooseSong(final String file) {
        new Thread(new Runnable(){
            public void run() {
            try {
                Clip clip = AudioSystem.getClip();
                clip.open(AudioSystem.getAudioInputStream(new File(file)));
                clip.start();
            }
            catch (Exception exc)
            {
                exc.printStackTrace(System.out);
            }
        }
    }).start();
    }
}
