/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clone_wars;

import java.util.ArrayList;
import java.io.BufferedReader;
import java.io.InputStreamReader;

/**
 *
 * @author andria_m
 */
public class room {

    Bounty_hunters luke = new Bounty_hunters();
    
    private int level = 1;
    protected int roomCap;
    protected String name;
    protected int hp;

    ArrayList<ennemy> ennemies = new ArrayList<ennemy>(1);
    BufferedReader dataIn = new BufferedReader(new InputStreamReader(System.in));

    
    public room() {
        factory();
        //luke.assassination(mob);
    }
    // la factory permettant d'ajouter des ennemies
    public void factory() {
        this.roomCap = (2 * this.level) + 1;
        System.out.println("Zone"+this.level);
        System.out.println("Roomcap: "+this.roomCap);
        addEnnemyIfEmpty(this.level);
    }

    // check if the room are empty
    public void addEnnemyIfEmpty(int level) {
        if (this.ennemies.isEmpty()) {
            this.ennemies.add(new ennemy(level));
            haveEnnemyAlive();
        }
    }
    
    public void haveEnnemyAlive() {
        // check if current mob is alive
        for (ennemy mob : this.ennemies) {
            this.hp = mob.hp;
            this.name = mob.name;
                startFight(luke, mob);
            if (mob.hp <= 0) {
                System.out.println("vous avez vaincu le " + mob.name + " !");
                this.ennemies.remove(mob);
                askToChangeRoom();
            }
        }
    }

    // ask to change room
    public void askToChangeRoom() {
        checkcapRoom();
        try {
            System.out.println("Tapez : '(f)ight' pour tabasser un ennemi Tapez: '(r)un' pour passer à la salle suivante ");
            String response = dataIn.readLine();
            if (response.contentEquals("f") && this.roomCap >= 0) {
                addEnnemyIfEmpty(this.level);
                this.roomCap--;
                System.out.println(this.roomCap + " Ennemies left to go!!");
            }
            if (response.contentEquals("r")) {
                this.roomCap = 0;
                upLevel();
                System.out.println("Welcome to the zone " + this.level);
                factory();
            }
            
        } catch (Exception e) {
            System.out.println(e);
        }
    }
    
    //check room
    public void checkcapRoom() {
        if(this.roomCap == 0) {
            upLevel();
            System.out.println("Well done, you just accessed the next zone");
            //System.out.println(this.hp);
            //System.out.println(this.name);
        }
    }

    
    public void startFight(Player player, ennemy mob){
        try{
            String action = dataIn.readLine();
            if(action.contentEquals("attack")){
                player.armedAttack(mob);
                player.xp += mob.xp;
            }
            else
                System.out.println("A mistake in your action");
        } catch(Exception e){
            System.err.println(e);
        }
    }
    
    /**
     * @return the level
     */
    public int getLevel() {
        return this.level;
    }
    
    private void upLevel(){
        this.level++;
        this.roomCap = (2 * this.level) + 1;
    }

    /**
     * @param level the levelRoom to set
     */
    public void setLevelRoom(int level) {
        this.level = level;
    }
}
