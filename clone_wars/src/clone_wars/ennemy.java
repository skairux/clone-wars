/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clone_wars;
import java.util.Random;
import java.lang.Math;
/**
 *
 * @author YannAssah
 */
public class ennemy {
    // variable temporaire 
    protected String            name;
    protected int                 hp;
    protected int                def;
    protected int              power;
    protected int              force;
    protected int       intelligence;
    protected int     current_ennemy;
    protected int          level = 1;
    protected int          xp = 30;
    
    
    public ennemy(int level) {
//        super();
        randEnnemy(level);
    }
    
    public void Clone(int level) {
        // hp * 1.5 puissance level pour les niveau
        this.name         = "Clone";
        this.hp           =     100;
        this.def          =     100;
        this.power        =     100;
        this.force        =       0;
        this.intelligence =     100;
        this.level        =   level;
    }
    
    public void Soldat(int level) {
        this.name         = "Soldat";
        this.hp           =     100;
        this.def          =     150;
        this.power        =     125;
        this.force        =       0;
        this.intelligence =     125;
        this.level        =   level;
    }
    
    public void Droid(int level) {
        this.name         = "Droid";
        this.hp           =      50;
        this.def          =     100;
        this.power        =      25;
        this.force        =       0;
        this.intelligence =     200;
        this.level        =   level;
    }

    public void setHp(int hp){
        if(hp >= 0)
            this.hp = hp;
        else
            this.hp = 0;
    }
    
    protected void setHp(ennemy ennemy, int hp){
        if(hp >= 0)
            ennemy.hp = hp;
        else
            ennemy.hp = 0;
    }

    //Getter methods 
    // norme of nothing, B*tch !
    public void getStats(){
        System.out.println("HP : " + this.hp);
        System.out.println("Def : " + this.def);
        System.out.println("Power : " + this.power);
        System.out.println("Force : " + this.force);
        System.out.println("Intelligence : " + this.intelligence);
        System.out.println("Lvl : " + this.level);
    }
    
    public void getStats(ennemy ennemy){
        System.out.println("HP : "           + ennemy.hp);
        System.out.println("Def : "          + ennemy.def);
        System.out.println("Power : "        + ennemy.power);
        System.out.println("Force : "        + ennemy.force);
        System.out.println("Intelligence : " + ennemy.intelligence);
        System.out.println("Lvl : "          + ennemy.level);
    }

    public int getHp(){
        return this.hp;
    }

    public int getHp(ennemy ennemy){
        return ennemy.hp;
    }

    /**
     *
     * @return
     */
    
    // check if ennemy getAttacked
   public boolean getAttacked() {
        return true;
    }
    
    public void randEnnemy(int level) {
        Random proba    = new Random();
        
        int scoreClone  = proba.nextInt(60);
        int scoreSoldat = proba.nextInt(30);
        int scoreDroid  = proba.nextInt(10);
        int max         = Math.max(scoreClone,Math.max(scoreSoldat, scoreDroid));

        if (max >= 31 && max < 60 ){
            Clone(level);
        }
        if(max >= 11 && max <= 30) {
            Soldat(level);
        }
        if (max >= 0 && max <= 10) {
            Droid(level);
        }
    }
}
